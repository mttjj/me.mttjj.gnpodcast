certifi==2017.4.17
chardet==3.0.4
feedparser==5.2.1
idna==2.5
mutagen==1.38
requests==2.18.1
sh==1.12.14
urllib3==1.21.1
