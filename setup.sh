#!/bin/sh

cd "${0%/*}"
mkdir out
cd out
mkdir tmp
cd ..

python3 -m venv env
source env/bin/activate
pip install -r requirements.txt

read -n 1 -s -r -p "Press any key to exit"