# me.mttjj.gnpodcast
A script for downloading, tagging, and indexing the GoNintendo Podcast.

## Setup Steps
1. Clone this repo.
2. Run `setup.sh`.

## Other Notes
* run the script using `run.sh`
* or update the `etc/me.mttjj.gnpodcast.plist` file (if necessary) and run `etc/setup.sh` to schedule the job using `launchd`