import os
import feedparser
import shutil
import datetime
import time
import requests
from configparser import ConfigParser, ExtendedInterpolation
import sh
from mutagen.mp3 import MP3
from mutagen.id3 import ID3, APIC, TALB, TIT2, TRCK, TPE1, TPE2, TCON, error

current_directory_path = os.path.dirname(__file__)

def get_full_path(path):
    '''
    Returns the given path as a full path by prefixing it with the current directory path.
    '''
    return os.path.join(current_directory_path, path)

config = ConfigParser(interpolation=ExtendedInterpolation())
config.read(get_full_path('config.ini'))

general = config['general']
date_format = general['date_format']
audio_file_format = general['audio_file_format']
audio_file_prefix = general['audio_file_name_prefix']

urls = config['urls']
rss_feed_url = urls['rss_feed']
pokeapi_url = urls['pokeapi']

files = config['file-paths']
index_file_path = get_full_path(files['index_file'])
temp_directory_path = files['temp_directory']
out_directory_path = files['out_directory']

def get_last_show_data():
    '''
    Retrieves the last podcast's show number and date from the given index file path.
    '''
    last_show_data = open(index_file_path).readlines()[-9:]

    last_show_num = last_show_data[0]
    last_show_date = datetime.datetime.strptime(last_show_data[1].strip(), date_format)

    return last_show_num, last_show_date

def get_show_date(last_show_date):
    '''
    Returns the date of the current show given the date of the last show.
    '''
    return (last_show_date + datetime.timedelta(weeks=1)).strftime(date_format)

def process_rss_feed():
    '''
    Returns the show's summary and mp3 file url from the RSS feed.
    '''
    rss_feed = feedparser.parse(rss_feed_url)
    file_url = rss_feed.entries[0].enclosures[0].href
    show_summary = rss_feed.entries[0].content[0]['value'][11:]

    return show_summary, file_url

def download_podcast_file(file_url, file_disk_path):
    '''
    Downloads the podcast file from the given url to the given disk path.
    '''
    with open(file_disk_path, 'wb') as f:
        f.write(requests.get(file_url).content)

def add_MP3_tags(file_disk_path, show_num):
    '''
    Adds MP3 tags to the file found at the given disk path.
    '''
    podcast_file = MP3(file_disk_path, ID3=ID3)

    # add ID3 tag if it doesn't exist
    try:
        podcast_file.add_tags()
    except error:
        pass

    with open(get_full_path(files['album_art']), "rb") as image_file:
        album_art_bytes = image_file.read()

    podcast_file.tags.add(TIT2(encoding=3, text='GoNintendo Podcast Webisode ' + str(show_num))) #title
    podcast_file.tags.add(TALB(encoding=3, text='GoNintendo Podcast')) #album
    podcast_file.tags.add(TPE1(encoding=3, text='Rawmeat Cowboy')) #artist
    podcast_file.tags.add(TPE2(encoding=3, text='Rawmeat Cowboy')) #album artist
    podcast_file.tags.add(TRCK(encoding=3, text=str(show_num))) #track number
    podcast_file.tags.add(TCON(encoding=3, text='Podcast')) #genre
    podcast_file.tags.add(APIC(encoding=3, mime='image/png', type=3, desc=u'Cover', data=album_art_bytes)) #artwork

    podcast_file.save()

def get_show_runtime(file_disk_path):
    '''
    Returns the runtime of the mp3 file specified by the given file path.
    '''
    m, s = divmod(MP3(file_disk_path, ID3=ID3).info.length, 60)
    h, m = divmod(m, 60)
    return '%d.%02d.%02d' % (h, m, round(s))

def get_show_pokemon(pokemon_num):
    '''
    Returns the name of the pokemon for the given number.
    '''
    return requests.get(pokeapi_url + pokemon_num).json()['name'].title()

def gather_podcast_info(num, date, runtime, pokemon, summary):
    '''
    Returns a tuple of the podcast info including number, date, runtime, pokemon, cast, and summary.
    '''
    cast = 'RMC,'
    empty_line = ''
    divider = '===================='

    return (empty_line, str(num), str(date), str(runtime), pokemon, cast, empty_line, summary, empty_line, divider)

def update_index_file(podcast_info):
    '''
    Updates the index file at the given path with the given podcast info.
    '''
    with open(index_file_path, 'a') as f:
        f.write('\n'.join(podcast_info))

def commit_to_git(show_num):
    '''
    Commits and pushes the changes to the index file to the git repo.
    '''
    git = sh.git.bake(_cwd=current_directory_path)
    git.add(files['index_file'])
    git.commit(m='-updating for show ' + str(show_num))
    git.push()

if __name__ == '__main__':
    last_show_num, last_show_date = get_last_show_data()
    show_num = int(last_show_num) + 1
    show_date = get_show_date(last_show_date)

    show_summary, file_url = process_rss_feed()

    file_disk_path = get_full_path(temp_directory_path + audio_file_prefix + str(show_num) + audio_file_format)
    download_podcast_file(file_url, file_disk_path)
    add_MP3_tags(file_disk_path, show_num)

    show_runtime = get_show_runtime(file_disk_path)
    show_pokemon = get_show_pokemon(str(show_num))

    podcast_info = gather_podcast_info(show_num, show_date, show_runtime, show_pokemon, show_summary)

    update_index_file(podcast_info)

    file_destination_path = get_full_path(out_directory_path + audio_file_prefix + str(show_num) + audio_file_format)
    shutil.move(file_disk_path, file_destination_path)

    commit_to_git(show_num)